from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ('recipient', 'sender', 'title', 'message')

        widget = {
            'recipient' : forms.TextInput(attrs={'class' : 'form-control'}),
            'sender' : forms.TextInput(attrs={'class' : 'form-control'}),
            'title' : forms.TextInput(attrs={'class' : 'form-control'}),
            'message' : forms.Textarea(attrs={'class' : 'form-control'}),
        }
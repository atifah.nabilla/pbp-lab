from django.db import models

# Create your models here.
class Note(models.Model):
    recipient = models.CharField(max_length=50)
    sender = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    message = models.TextField(blank = True)
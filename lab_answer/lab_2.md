### **Apakah perbedaan antara JSON dan XML?**
JSON dan XML merupakan format untuk transfer data. Walaupun fungsinya sama, terdapat perbedaan antara keduanya. Berikut adalah perbedaanya.
- XML merupakan sebuah markup language yang memiliki tag dalam penulisan, sementara JSON adalah sebuah format yang berbasis Javascript.
- XML disimpan dengan struktur seperti HTML atau bisa dikatakan tree structure, sementara JSON disimpan seperti map, ada key dan value.
- Ukuran file XML lebih besar daripada JSON, sehingga transmisi data XML lebih lama daripada JSON.
- XML bisa memuat lebih banyak varian tipe data dibandingkan JSON. XML bisa menyimpan tipe data non-primitif, sementara JSON hanya dapat menyimpan data primitif. 
- XML menjadi rentan terhadap serangan karena perluasan entitas eksternal dan DTD diaktifkan, sementara JSON akan rentan jika JSONP digunakan.

### **Apakah perbedaan antara HTML dan XML?**
- XML dan HTML sama-sama merupakan markup language, tetapi HTML berfungsi untuk menampilkan desain web kepada pengguna, sementara XML hanya berfungsi sebagai pengantar data dari database ke pengguna. 
- Dari segi syntax, HTML memiliki aturannya tersendiri, contoh: p untuk paragraf, h1-h6 untuk header, head, body, dan lain-lain. XML dibuat sesuai kebutuhan projek dan bersifat deskriptif, contoh: `<to>Rhoma</to>`. 
- Selain itu, tag pada XML bersifat sensitif, sementara HTML tidak. 
- Closing tag pada XML merupakan suatu keharusan, tetapi HTML tidak selalu harus menyertakan closing tag.